FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install -y software-properties-common vim
RUN add-apt-repository -y ppa:jonathonf/python-3.6
RUN apt-get update

RUN apt-get install -y build-essential python3.6 python3.6-dev python3-pip python3.6-venv
RUN apt-get install -y git

# update pip
RUN python3.6 -m pip install pip --upgrade
RUN python3.6 -m pip install wheel

WORKDIR /home

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY app.py app.py
COPY boot.sh boot.sh
RUN chmod +x boot.sh

EXPOSE 5000
ENV LC_ALL "C.UTF-8"
ENV FLASK_APP "app.py"
ENV LANG "C.UTF-8"
#RUN export LC_ALL=C.UTF-8 && export LANG=C.UTF-8 && export FLASK_APP=app.py
CMD ["python3.6", "-m", "flask", "run", "--host=0.0.0.0"]
#ENTRYPOINT ["./boot.sh"]

